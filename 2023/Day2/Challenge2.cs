﻿namespace AdventOfCode.Year2023.Day2
{
    public static class Challenge2
    {
        public enum CubeColors
        { 
            Red = 1,
            Blue = 2,
            Green = 3
        }
        public static List<CubeColors> AllCubeColors = Enum.GetValues(typeof(CubeColors)).Cast<CubeColors>().ToList();

        public class Game
        {
            public int Id { get; set; }

            public List<Round> Rounds { get; set; }
            public Dictionary<CubeColors, int> MaxReveals { get; set; }
            public int Power { get; set; }

            // gameData format - Game {Id}: {roundData}; {roundData}; ... {roundData}
            public Game(string gameData)
            {
                var roundSplit = gameData.Split(':');
                var gameString = roundSplit[0];
                Id = int.Parse(gameString.Replace("Game ", string.Empty));

                var allRoundsString = roundSplit[1];
                var roundsStrings = allRoundsString.Split(";");
                Rounds = roundsStrings
                    .Select(r => new Round(r))
                    .ToList();

                MaxReveals = AllCubeColors
                    .Select(c => 
                        (Key: c, 
                        Value: Rounds
                            .SelectMany(r => r.Reveals)
                            .Where(v => v.Key == c)
                            .Select(v => v.Value)
                            .Max())
                        )
                    .ToDictionary(p => p.Key, p => p.Value);

                Power = MaxReveals
                    .Select(r => r.Value)
                    .Aggregate(1, (a, b) => a * b);
            }
        }

        public class Round 
        {
            public int Id { get; set; }

            public Dictionary<CubeColors, int> Reveals { get; set; }

            // roundData format - {int} {color}, {int} {color}, ... {int} {color}
            public Round(string roundData)
            {
                var reveals = roundData
                    .TrimStart()
                    .Split(", ");

                Reveals = reveals
                    .Select(r =>
                    {
                        var split = r.Split(" ");
                        return ( Key: Enum.Parse<CubeColors>(split[1], true), Value: int.Parse(split[0]));
                    })
                    .ToDictionary(p => p.Key, p => p.Value);
            }
        }

        public static void Run()
        {
            List<string> inputLines = Input.GameData.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

            // break input into games and rounds
            // spin through each game's rounds, and find max of each color needed for the game
            var games = inputLines.Select(l => new Game(l));

            var idSum = games.Sum(g => g.Power);

            Console.WriteLine($"Challenge 2: { idSum }");
        }
    }
}
