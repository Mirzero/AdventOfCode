﻿namespace AdventOfCode.Year2023.Day2
{
    public class Program
    {
        public static void Main()
        {
            Challenge1.Run();
            Challenge2.Run();
        }
    }
}
