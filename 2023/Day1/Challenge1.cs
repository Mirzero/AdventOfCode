﻿namespace AdventOfCode.Year2023.Day1
{
    public static class Challenge1
    {
        public static void Run()
        {
            List<string> calibrationLines = Input.CalibrationData.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

            var calibrationDigits = calibrationLines
                .Select(c => new string(c.Where(char.IsDigit).ToArray()))
                .Where(c => !string.IsNullOrWhiteSpace(c));

            var calibrationValues = calibrationDigits
                .Select(c => string.Concat(c.First().ToString(), c.Last().ToString()));

            var calibrationSum = calibrationValues.Sum(v => int.Parse(v));

            Console.WriteLine("Challenge 1: " + calibrationSum);
        }
    }
}
