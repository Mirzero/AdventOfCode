﻿namespace AdventOfCode.Year2023.Day1
{
    public static class Challenge2
    {
        public class CalibrationExtract
        {
            public string Input { get; set; }
            public int? First { get; set; }
            public int? Last { get; set; }
            public int? Value { get; set; }
        }

        public static List<(string Text, int Value)> NumberStrings = new List<(string Text, int Value)>
        {
            ("1", 1),
            ("one", 1),
            ("2", 2),
            ("two", 2),
            ("3", 3),
            ("three", 3),
            ("4", 4),
            ("four", 4),
            ("5", 5),
            ("five", 5),
            ("6", 6),
            ("six", 6),
            ("7", 7),
            ("seven", 7),
            ("8", 8),
            ("eight", 8),
            ("9", 9),
            ("nine", 9),
        };

        public static List<(string Text, int Value)> NumberStringsReversed = NumberStrings
            .Select(d => (d.Text.ReverseString(), d.Value))
            .ToList();

        public static void Run()
        {
            var data = Input.CalibrationData
                .Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                .Select(l => new CalibrationExtract { Input = l })
                .ToList();

            data.ForEach(i =>
            {
                i.First = Extract(i.Input, NumberStrings);
                i.Last = Extract(i.Input.ReverseString(), NumberStringsReversed);
                i.Value = i.First.HasValue && i.Last.HasValue
                    ? i.First * 10 + i.Last
                    : null;
            });

            var sum = data.Sum(d => d.Value ?? 0);

            Console.WriteLine("Challenge 2: " + sum);
        }

        public static int? Extract(string input, List<(string Text, int Value)> keywords)
        {
            var keywordIndices = keywords
                .Select(k => (Index: input.IndexOf(k.Text), Value: k.Value))
                .Where(k => k.Index > -1)
                .OrderBy(k => k.Index);

            if (!keywordIndices.Any())
                return null;

            return keywordIndices
                .FirstOrDefault()
                .Value;
        }

        public static string ReverseString(this string victim)
        {
            return new string(victim.Reverse().ToArray());
        }
    }
}
