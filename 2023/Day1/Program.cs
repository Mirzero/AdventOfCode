﻿namespace AdventOfCode.Year2023.Day1
{
    public class Program
    {
        public static void Main()
        {
            Challenge1.Run();
            Challenge2.Run();
        }
    }
}
