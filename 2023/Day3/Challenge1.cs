﻿namespace AdventOfCode.Year2023.Day3
{
    public enum Direction
    {
        Above,
        Below
    }

    public class Schematic
    {
        public Component[,] Components { get; set; }
        public (int X, int Y) Dimensions { get; set; }

        public Schematic(List<string> inputLines)
        {
            var input = string.Join(string.Empty, inputLines);
            Dimensions = (inputLines.First().Length, inputLines.Count);

            Components = new Component[Dimensions.X, Dimensions.Y];

            input
                .Select((c, i) => (Value: c, Index: i))
                .ToList()
                .ForEach(ci => 
                {
                    var x = ci.Index % Dimensions.X;
                    var y = ci.Index / Dimensions.X;
                    Components[y, x] = new Component(ci.Value, x, y);
                } );
        }
    }

    public class Component
    { 
        public char Value { get; set; }
        public (int X, int Y) Coordinates { get; set; }
        public bool AdjacencyProvider { get; set; }

        public Component(char value, int x, int y)
        {
            Value = value;
            Coordinates = (x, y);
            AdjacencyProvider = !char.IsDigit(Value) && Value != '.'; // i.e. is non-. symbol
        }
    }

    public static class Challenge1
    {
        public static int Run(List<string> inputLines)
        {
            var schematic = new Schematic(inputLines);

            var numbersWithAdjacency = new List<(int Value, (int X, int Y) Coordinates)>();

            string numStack = string.Empty;
            var adjacencyPropagation = false;

            for (int y = 0; y < schematic.Components.GetLength(0); y++)
            {
                for (int x = 0; x < schematic.Components.GetLength(1); x++)
                {
                    var current = schematic.Components[y, x];

                    var currentAdjacencyProvider = schematic.GetNeighbor(current, Direction.Above)?.AdjacencyProvider ?? false;
                    currentAdjacencyProvider |= current.AdjacencyProvider;
                    currentAdjacencyProvider |= schematic.GetNeighbor(current, Direction.Below)?.AdjacencyProvider ?? false;

                    adjacencyPropagation |= currentAdjacencyProvider;

                    if (char.IsDigit(current.Value))
                    {
                        numStack += current.Value;
                    }
                    else
                    {
                        if(!string.IsNullOrEmpty(numStack) && adjacencyPropagation)
                            numbersWithAdjacency.Add((Value: int.Parse(numStack), Coordinates: current.Coordinates));

                        numStack = string.Empty;
                        adjacencyPropagation = currentAdjacencyProvider;
                    }
                }
            }

            return numbersWithAdjacency.Select(n => n.Value).Sum();
        }

        private static Component? GetNeighbor(this Schematic schematic, Component component, Direction direction)
        {
            if (direction == Direction.Above)
            {
                if (component.Coordinates.Y == 0)
                    return null;

                return schematic.Components[component.Coordinates.Y - 1, component.Coordinates.X];
            }

            if (direction == Direction.Below)
            {
                if (component.Coordinates.Y >= schematic.Dimensions.Y - 1)
                    return null;

                return schematic.Components[component.Coordinates.Y + 1, component.Coordinates.X];
            }

            return null;
        }
    }
}
