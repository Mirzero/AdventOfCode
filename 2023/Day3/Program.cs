﻿namespace AdventOfCode.Year2023.Day3
{
    public class Program
    {
        public static void Main()
        {
            List<string> inputLines = Input.Data.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

            Console.WriteLine($"Challenge 1: { Challenge1.Run(inputLines) }");
            Console.WriteLine($"Challenge 2: { Challenge2.Run(inputLines) }");
        }
    }
}
